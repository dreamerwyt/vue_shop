import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login.vue'
import Home from '@/components/home.vue'
import Welcome from '@/components/Welcome.vue'
import Users from '@/components/user/Users.vue'
import Rights from '@/components/power/Rights.vue'
import Roles from '@/components/power/Roles.vue'
import Cate from '@/components/goods/Cate.vue'

Vue.use(Router)

const router=new Router ({
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
      redirect:'/welcome',
      children:[
        {
          path:'/welcome',
          name:'welcome',
          component:Welcome
        },
        {
          path:'/users',
          name:'users',
          component:Users
        },
        {
          path:'/rights',
          name:'rights',
          component:Rights
        },
        {
          path:'/roles',
          name:'roles',
          component:Roles
        },
        {
          path:'/categories',
          name:'categories',
          component:Cate
        }      
      ]
    }
  ]
});

// 挂载路由导航守卫
router.beforeEach((to,from,next) =>{
  // to将要访问的路径
  // from代表从哪个路径跳转而来
  // next是一个函数,表示放行
  // next() 放行 next('/login') 强制放行
  if(to.path==='/login') return next();
  // 获取token
  const tokenStr=window.sessionStorage.getItem("token");
  if(!tokenStr) return next('/login')
  next();
});

export default router;
